package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    List<User> findAll();

    @NotNull
    User add(@NotNull User user);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @NotNull
    User removeUser(@NotNull User user);

    @Nullable
    User removeById(@NotNull String id);

    @Nullable
    User removeByLogin(@NotNull String login);

    @Nullable
    User removeByEmail(@NotNull String email);

    void add(@NotNull List<User> users);

    void add(@NotNull User... users);

    void clear();

    void load(@NotNull List<User> users);

    void load(@NotNull User... users);

}
