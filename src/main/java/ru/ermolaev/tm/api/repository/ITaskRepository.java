package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(@NotNull String userId, @NotNull Task task);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    void remove(@NotNull String userId, @NotNull Task task);

    void clear(@NotNull String userId);

    @NotNull
    Task findById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task removeByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task add(@NotNull Task task);

    void add(@NotNull List<Task> tasks);

    void add(@NotNull Task... tasks);

    void clear();

    void load(@NotNull List<Task> tasks);

    void load(@NotNull Task... tasks);

    @NotNull
    List<Task> getTasksList();

}
