package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void add(@NotNull String userId, @NotNull Project project);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    void remove(@NotNull String userId, @NotNull Project project);

    void clear(@NotNull String userId);

    @NotNull
    Project findById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project add(@NotNull Project project);

    void add(@NotNull List<Project> projects);

    void add(@NotNull Project... projects);

    void clear();

    void load(@NotNull List<Project> projects);

    void load(@NotNull Project... projects);

    @NotNull
    List<Project> getProjectsList();

}
