package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void createProject(@Nullable String userId, @Nullable String name);

    void createProject(@Nullable String userId, @Nullable String name, String description);

    void addProject(@Nullable String userId, @Nullable Project project);

    @NotNull
    List<Project> showAllProjects(@Nullable String userId);

    void removeProject(@Nullable String userId, @Nullable Project project);

    void removeAllProjects(@Nullable String userId);

    @NotNull
    Project findProjectById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project findProjectByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project findProjectByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project updateProjectById(@Nullable String userId, @Nullable String id, @Nullable String name, String description);

    @NotNull
    Project updateProjectByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, String description);

    @NotNull
    Project removeProjectById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project removeProjectByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project removeProjectByName(@Nullable String userId, @Nullable String name);

    void load(@Nullable List<Project> projects);

    void load(@Nullable Project... projects);

    @NotNull
    List<Project> getProjectsList();

}
