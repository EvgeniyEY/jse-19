package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void createTask(@Nullable String userId, @Nullable String name);

    void createTask(@Nullable String userId, @Nullable String name, String description);

    void addTask(@Nullable String userId, @Nullable Task task);

    @NotNull
    List<Task> showAllTasks(@Nullable String userId);

    void removeTask(@Nullable String userId, @Nullable Task task);

    void removeAllTasks(@Nullable String userId);

    @NotNull
    Task findTaskById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task findTaskByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task findTaskByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, String description);

    @NotNull
    Task updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, String description);

    @NotNull
    Task removeTaskById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task removeTaskByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task removeTaskByName(@Nullable String userId, @Nullable String name);

    void load(@Nullable List<Task> tasks);

    void load(@Nullable Task... tasks);

    @NotNull
    List<Task> getTasksList();

}
