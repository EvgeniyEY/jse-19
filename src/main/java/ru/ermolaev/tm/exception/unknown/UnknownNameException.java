package ru.ermolaev.tm.exception.unknown;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.exception.AbstractException;

public final class UnknownNameException extends AbstractException {

    public UnknownNameException() {
        super("Error! This name does not exist.");
    }

    public UnknownNameException(@NotNull final String name) {
        super("Error! This name [" + name + "] does not exist.");
    }

}
