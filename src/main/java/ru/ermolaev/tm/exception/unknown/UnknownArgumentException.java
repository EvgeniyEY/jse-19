package ru.ermolaev.tm.exception.unknown;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.exception.AbstractException;

public final class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! This argument does not exist.");
    }

    public UnknownArgumentException(@NotNull final String argument) {
        super("Error! This argument [" + argument + "] does not exist.");
    }

}
