package ru.ermolaev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.TerminalUtil;

public final class UserPasswordUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "user-update-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user password.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updatePassword(newPassword);
        System.out.println("[PASSWORD CHANGED]");
        serviceLocator.getAuthenticationService().logout();
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
