package ru.ermolaev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.user.AlreadyLoggedInException;
import ru.ermolaev.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login in your account.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOGIN]");
        if (serviceLocator.getAuthenticationService().isAuthenticated()) throw new AlreadyLoggedInException();
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().login(login, password);
        System.out.println("[OK]");
    }

}
