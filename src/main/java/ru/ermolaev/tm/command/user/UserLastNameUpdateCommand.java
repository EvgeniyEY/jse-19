package ru.ermolaev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.TerminalUtil;

public final class UserLastNameUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "user-update-last-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user last name.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER NEW USER LAST NAME:");
        @Nullable final String newLastName = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updateUserLastName(newLastName);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
