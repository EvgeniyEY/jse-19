package ru.ermolaev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.TerminalUtil;

public final class UserMiddleNameUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "user-update-middle-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user middle name.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER NEW USER MIDDLE NAME:");
        @Nullable final String newMiddleName = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updateUserMiddleName(newMiddleName);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
