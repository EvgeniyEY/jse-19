package ru.ermolaev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.util.TerminalUtil;

public final class UserRegistrationCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "registration";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Registration of new account.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().registration(login, password, email);
        System.out.println("[OK]");
    }

}
