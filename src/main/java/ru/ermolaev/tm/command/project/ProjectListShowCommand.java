package ru.ermolaev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

public final class ProjectListShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[PROJECTS LIST]");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        @NotNull final List<Project> projects = serviceLocator.getProjectService().showAllProjects(userId);
        int index = 1;
        for (@NotNull final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
