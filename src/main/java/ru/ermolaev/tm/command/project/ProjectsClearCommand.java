package ru.ermolaev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;

public final class ProjectsClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Delete all projects.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        serviceLocator.getProjectService().removeAllProjects(userId);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
