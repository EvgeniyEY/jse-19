package ru.ermolaev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-show-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by index.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        @Nullable Integer index = TerminalUtil.nextNumber();
        if (index == null) return;
        index = index - 1;
        @NotNull final Project project = serviceLocator.getProjectService().findProjectByIndex(userId, index);
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
