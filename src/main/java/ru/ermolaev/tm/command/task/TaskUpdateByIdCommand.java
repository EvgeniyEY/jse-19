package ru.ermolaev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().findTaskById(userId, id);
        System.out.println("ENTER NEW TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateTaskById(userId, id, name, description);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
