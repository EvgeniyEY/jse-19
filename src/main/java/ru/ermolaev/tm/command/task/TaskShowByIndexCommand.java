package ru.ermolaev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-show-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK INDEX:");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        @Nullable Integer index = TerminalUtil.nextNumber();
        if (index == null) return;
        index = index - 1;
        @NotNull final Task task = serviceLocator.getTaskService().findTaskByIndex(userId, index);
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
