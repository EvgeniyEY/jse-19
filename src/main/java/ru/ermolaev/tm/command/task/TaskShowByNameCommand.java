package ru.ermolaev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public final class TaskShowByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-show-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK NAME:");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        @Nullable final String name = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().findTaskByName(userId, name);
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
