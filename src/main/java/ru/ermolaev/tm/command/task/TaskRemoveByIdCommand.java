package ru.ermolaev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        @Nullable final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().removeTaskById(userId, id);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
