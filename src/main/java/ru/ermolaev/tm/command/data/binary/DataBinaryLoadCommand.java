package ru.ermolaev.tm.command.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import ru.ermolaev.tm.enumeration.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-bin-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from binary file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");

        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY);
             @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        }

        System.out.println("[OK]");
        serviceLocator.getAuthenticationService().logout();
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
