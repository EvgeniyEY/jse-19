package ru.ermolaev.tm.command.data.xml.jaxb;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataXmlJaxbClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-xml-jb-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove XML (Jax-B) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE XML (JAX-B) FILE]");
        @NotNull final File file = new File(DataConstant.FILE_XML_FX);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
