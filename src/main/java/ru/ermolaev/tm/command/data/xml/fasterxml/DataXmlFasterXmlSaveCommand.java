package ru.ermolaev.tm.command.data.xml.fasterxml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import ru.ermolaev.tm.enumeration.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public final class DataXmlFasterXmlSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-xml-fx-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to XML (fasterXML) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML (FASTERXML) SAVE]");
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        @NotNull final File file = new File(DataConstant.FILE_XML_FX);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xmlData = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(xmlData.getBytes());
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
