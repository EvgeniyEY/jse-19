package ru.ermolaev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractModel implements Serializable {

    private static final long serialVersionUID = 1001L;

    private String id = UUID.randomUUID().toString();

}
