package ru.ermolaev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.service.IAuthenticationService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.exception.user.AccessDeniedException;
import ru.ermolaev.tm.exception.user.AccessLockedException;
import ru.ermolaev.tm.exception.user.UserAlreadyExistException;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.util.HashUtil;

public final class AuthenticationService implements IAuthenticationService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthenticationService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuthenticated() {
        return userId != null;
    }

    @Override
    @SneakyThrows
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessLockedException();
        final String hash = HashUtil.hidePassword(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    @SneakyThrows
    public void registration(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (userService.findByLogin(login) != null) throw new UserAlreadyExistException();
        userService.create(login, password, email);
    }

    @Override
    @SneakyThrows
    public void updatePassword(@Nullable final String newPassword) {
        if (!isAuthenticated()) throw new AccessDeniedException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        userService.updatePassword(userId, newPassword);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findCurrentUser() {
        if (!isAuthenticated()) throw new AccessDeniedException();
        return userService.findById(userId);
    }

    @Override
    @SneakyThrows
    public void updateUserFirstName(@Nullable final String newFirstName) {
        if (!isAuthenticated()) throw new AccessDeniedException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyFirstNameException();
        userService.updateUserFirstName(userId, newFirstName);
    }

    @Override
    @SneakyThrows
    public void updateUserMiddleName(@Nullable final String newMiddleName) {
        if (!isAuthenticated()) throw new AccessDeniedException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyMiddleNameException();
        userService.updateUserMiddleName(userId, newMiddleName);
    }

    @Override
    @SneakyThrows
    public void updateUserLastName(@Nullable final String newLastName) {
        if (!isAuthenticated()) throw new AccessDeniedException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyLastNameException();
        userService.updateUserLastName(userId, newLastName);
    }

    @Override
    @SneakyThrows
    public void updateUserEmail(@Nullable final String newEmail) {
        if (!isAuthenticated()) throw new AccessDeniedException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyEmailException();
        userService.updateUserEmail(userId, newEmail);
    }

    @Override
    @SneakyThrows
    public void checkRole(@Nullable final Role [] roles) {
        if (roles == null || roles.length == 0) return;
        @NotNull final String userId = getUserId();
        @Nullable final User user = userService.findById(userId);
        if (user == null) return;
        final Role role = user.getRole();
        for (@Nullable final Role item: roles) if (role.equals(item)) return;
        throw new AccessDeniedException();
    }

}
