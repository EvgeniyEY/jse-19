package ru.ermolaev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.exception.empty.EmptyIdException;
import ru.ermolaev.tm.exception.empty.EmptyUserIdException;
import ru.ermolaev.tm.exception.incorrect.IncorrectIndexException;
import ru.ermolaev.tm.exception.empty.EmptyNameException;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    @SneakyThrows
    public void createProject(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    @SneakyThrows
    public void createProject(@Nullable final String userId, @Nullable final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    @SneakyThrows
    public void addProject(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> showAllProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    @Override
    @SneakyThrows
    public void removeProject(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    @SneakyThrows
    public void removeAllProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findProjectById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findByIndex(userId, index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = findProjectById(userId, id);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = findProjectByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project removeProjectById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project removeProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        projectRepository.load(projects);
    }

    @Override
    public void load(@Nullable final Project ... projects) {
        if (projects == null) return;
        projectRepository.load(projects);
    }

    @NotNull
    @Override
    public List<Project> getProjectsList() {
        return projectRepository.getProjectsList();
    }

}
