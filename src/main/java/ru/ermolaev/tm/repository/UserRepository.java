package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.repository.IUserRepository;
import ru.ermolaev.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    @NotNull
    private List<User> users = new ArrayList<>();

    @NotNull
    @Override
    public List<User> findAll() {
        return users;
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        users.add(user);
        return user;
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        for (@NotNull final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@NotNull final User user: users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        for (@NotNull final User user: users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @NotNull
    @Override
    public User removeUser(@NotNull final User user) {
        users.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeById(@NotNull final String id) {
        @Nullable final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return removeUser(user);
    }

    @Nullable
    @Override
    public User removeByEmail(@NotNull final String email) {
        @Nullable final User user = findByEmail(email);
        if (user == null) return null;
        users.remove(user);
        return removeUser(user);
    }

    @Override
    public void add(@NotNull final List<User> users) {
        for (@NotNull final User user: users) add(user);
    }

    @Override
    public void add(@NotNull final User... users) {
        for (@NotNull final User user: users) add(user);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public void load(@NotNull final List<User> users) {
        clear();
        add(users);
    }

    @Override
    public void load(@NotNull final User... users) {
        clear();
        add(users);
    }

}
