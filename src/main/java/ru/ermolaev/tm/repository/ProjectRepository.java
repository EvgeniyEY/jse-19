package ru.ermolaev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.exception.unknown.UnknownIdException;
import ru.ermolaev.tm.exception.unknown.UnknownIndexException;
import ru.ermolaev.tm.exception.unknown.UnknownNameException;
import ru.ermolaev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @Override
    public void clear(@NotNull final  String userId) {
        projects.removeAll(findAll(userId));
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        throw new UnknownIdException(id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        for (@NotNull final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (projects.indexOf(project) == index) return project;
        }
        throw new UnknownIndexException(index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        throw new UnknownNameException(name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Project project = findById(userId, id);
        projects.remove(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Project project = findByIndex(userId, index);
        projects.remove(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project project = findByName(userId, name);
        projects.remove(project);
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void add(@NotNull final List<Project> projects) {
        for (@NotNull final Project project: projects) add(project);
    }

    @Override
    public void add(@NotNull final Project... projects) {
        for (@NotNull final Project project: projects) add(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public void load(@NotNull final List<Project> projects) {
        clear();
        add(projects);
    }

    @Override
    public void load(@NotNull final Project... projects) {
        clear();
        add(projects);
    }

    @NotNull
    @Override
    public List<Project> getProjectsList() {
        return projects;
    }

}
