package ru.ermolaev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.exception.unknown.UnknownIdException;
import ru.ermolaev.tm.exception.unknown.UnknownIndexException;
import ru.ermolaev.tm.exception.unknown.UnknownNameException;
import ru.ermolaev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    @NotNull
    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        if (!userId.equals(task.getUserId())) return;
        tasks.remove(task);
    }

    @Override
    public void clear(@NotNull final String userId) {
        tasks.removeAll(findAll(userId));
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Task task: tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        throw new UnknownIdException(id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        for (@NotNull final Task task: tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (tasks.indexOf(task) == index) return task;
        }
        throw new UnknownIndexException(index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Task task: tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        throw new UnknownNameException(name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Task task = findById(userId, id);
        tasks.remove(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Task task = findByIndex(userId, index);
        tasks.remove(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Task task = findByName(userId, name);
        tasks.remove(task);
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void add(@NotNull final List<Task> tasks) {
        for (@NotNull final Task task: tasks) add(task);
    }

    @Override
    public void add(@NotNull final Task... tasks) {
        for (@NotNull final Task task: tasks) add(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public void load(@NotNull final List<Task> tasks) {
        clear();
        add(tasks);
    }

    @Override
    public void load(@NotNull final Task... tasks) {
        clear();
        add(tasks);
    }

    @NotNull
    @Override
    public List<Task> getTasksList() {
        return tasks;
    }

}
