package ru.ermolaev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.ermolaev.tm.api.repository.ICommandRepository;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        initCommands();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.ermolaev.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.ermolaev.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            commandList.add(clazz.newInstance());
        }
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}
